package application.projet;

import android.net.Uri;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceURL {
    private static final String HOST = "https://demo-lia.univ-avignon.fr/cerimuseum/";
    private static final String PATH_1 = "api";
    private static final String PATH_2 = "v1";
    private static final String PATH_3 = "json";
    private static final String PATH_4 = "1";

    public static Uri.Builder commonBuilder(){
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1)
                .appendPath(PATH_2)
                .appendPath(PATH_3)
                .appendPath(PATH_4);
        return builder;
    }

    private static String IDS_PATH = "ids/";
    public static URL buildSearchIds() throws MalformedURLException{
        Uri.Builder builder = commonBuilder();
        builder.appendPath(IDS_PATH);
        return new URL(builder.build().toString());
    }

    //obtenir les informations pour un objet donné
    private static String ITEM_PATH = "items/";
    private static String SEARCH_PARAM = "id";
    private static URL buildSearchObject(String object) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ITEM_PATH)
                .appendQueryParameter(SEARCH_PARAM, object);
        return new URL(builder.build().toString());
    }



}

