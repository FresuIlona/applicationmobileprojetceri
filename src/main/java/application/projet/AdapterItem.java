package application.projet;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.view.LayoutInflater;
import android.widget.ListView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class AdapterItem extends ArrayAdapter<Item> {
    private Activity activity;
    private ArrayList<Item> items;
    private static LayoutInflater in = null;

    public AdapterItem(Activity a, int textviewRessourceId, ArrayList<Item> items){
        super(a, textviewRessourceId, items);
        this.activity = a;
        this.items = items;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View v = null;
        if(convertView == null) {
            in = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = in.inflate(R.layout.content_main, null);
        }
        TextView nameLV = (TextView) v.findViewById(R.id.textview);
        TextView brandTV = (TextView) v.findViewById(R.id.textview1);
        TextView category = (TextView) v.findViewById(R.id.textview2);

        nameLV.setText(getItem(position).getName());
        brandTV.setText(getItem(position).getBrand());
        category.setText(getItem(position).getCategory());
        return v;
    }
}
