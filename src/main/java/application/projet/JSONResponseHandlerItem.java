package application.projet;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.util.JsonReader;
import android.util.JsonToken;

public class JSONResponseHandlerItem {
    private static final String TAG = JSONResponseHandlerItem.class.getSimpleName();
    private Item item;
    protected ArrayList<Item> items = new ArrayList<>();
    protected Map<String, String> mapItem = new HashMap<>();

    public JSONResponseHandlerItem(Item item){
        this.item = item;
    }

    public Map<String,String> readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try{
            mapItem = readItems(reader);
            return mapItem;
        } finally {
            reader.close();
        }
    }

    public Map<String, String> readItems(JsonReader reader)throws IOException{
        reader.beginObject();
        while(reader.hasNext()){
            String name = reader.nextName();
            if(name.equals("items")){
                mapItem = readArrayItems(reader);
            }
            else {
                reader.skipValue();
            }
            reader.endObject();
        }
        return mapItem;
    }

    public String[] nextArrayString(JsonReader reader) throws IOException{
        String [] arrayString = new String[30];
        reader.beginArray();
        int i = 0;
        while (reader.hasNext()){
            arrayString[i] = reader.nextString();
            i++;
        }
        reader.endArray();
        return arrayString;
    }

    public int[] nextArrayInt(JsonReader reader) throws IOException{
        int [] arrayInt = new int[30];
        reader.beginArray();
        int i = 0;
        while (reader.hasNext()){
            arrayInt[i] = reader.nextInt();
            i++;
        }
        reader.endArray();
        return arrayInt;
    }

    public Map<String, String> readArrayItems(JsonReader reader) throws IOException{
        reader.beginArray();
        int nb = 0;
        while (reader.hasNext()){
            item = null;
            reader.beginObject();
            while (reader.hasNext()){
                String name = reader.nextName();
                if(nb == 0){
                    if (name.equals("name")) item.setName(reader.nextString());
                    else if (name.equals("categories")) item.setCategory(nextArrayString(reader));
                    else if(name.equals("description")) item.setDescription(reader.nextString());
                    else if(name.equals("timeFrame")) item.setTimeFrame(nextArrayInt(reader));
                    else if(name.equals("year") && reader.peek() != JsonToken.NULL){
                        item.setYear(reader.nextInt());
                    }
                    else if(name.equals("brand") && reader.peek() != JsonToken.NULL) item.setBrand(reader.nextString());
                    else if(name.equals("technicalDetails") && reader.peek() != JsonToken.NULL) item.setTechnicalDetails(nextArrayString(reader));
                    else if(name.equals("working") && reader.peek() != JsonToken.NULL) item.setWorking(reader.nextBoolean());
                    //else if(name.equals("pictures") && reader.peek() != JsonToken.NULL) item.setPictures(nextMap(reader));
                    else{
                        reader.skipValue();
                    }
                } else {
                    reader.skipValue();
                }

                if(item != null) mapItem.put(item.getName(), item.getBrand());
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
        return mapItem;
    }


}
