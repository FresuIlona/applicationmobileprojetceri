package application.projet;

import java.util.Map;

public class Item {
    private String name;
    private String[] category;
    private String description;
    private int[] timeFrame;

    private int year;
    private String brand;
    private String[] technicalDetails;
    private boolean working;
    private Map<String, String> pictures;

    public Item(String name, String[] category, String description, int[] timeFrame){
        this.name = name;
        this.category = category;
        this.description = description;
        this.timeFrame = timeFrame;
    }

    public Item(String name, String[] category, String description, int[] timeFrame, int year, String brand, String[] technicalDetails, boolean working, Map<String, String> pictures){
        this.name = name;
        this.category = category;
        this.description = description;
        this.timeFrame = timeFrame;
        this.year = year;
        this.brand = brand;
        this.technicalDetails = technicalDetails;
        this.working = working;
        this.pictures = pictures;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        String c = "";
        for (int i = 0; i <= 3; i++){
            c = c + ", " + category[i];
        }
        return c;
    }

    public void setCategory(String[] category) {
        this.category = category;
    }

    public int[] getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(int[] timeFrame) {
        this.timeFrame = timeFrame;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String[] getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(String[] technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public Map<String, String> getPictures() {
        return pictures;
    }

    public void setPictures(Map<String, String> pictures) {
        this.pictures = pictures;
    }
}
